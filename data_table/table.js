/*
 *
 * comparitors for sort 
 *
 */
Compare = {};
Compare.basicCompare = function(a,b){
	return a == b ? 0 : a > b ? 1 : -1;
};
Compare.salaryCompare = function(a, b){
	return a.replace("$", "").replace(/,/g, "") - b.replace("$", "").replace(/,/g, "");
};
Compare.numberCompare = function(a, b){
	return a-b;
};
Compare.startDateCompare = function(a, b){
	if(a == b) return 0; // same whole string
	var pa = a.split("/");
	var pb = b.split("/");
	if(pa[0] == pb[0]){// same years
		if(pa[1] == pb[1]) {// same months
			return pa[2] - pb[2]; // compare dates
		}else {// different months
			return pa[1] - pb[1]; // compare months
		}	
	}else{// different years
		return pa[0] - pb[0];
	}
};


/**
 *
 * even handling functions
 *
 */
// handle sort
function handleSort(evt){
	var tgt = evt.srcElement;
	console.log("header clicked:"+evt.srcElement);
	
	// change css class
	var cssClass = tgt.className;
	var asc = false;
	if(cssClass.indexOf("asc") >= 0){// ascending
		cssClass = cssClass.replace("asc", "desc");
		asc = false;
	}else {// descending or default
		cssClass = cssClass.replace("desc", "asc").replace("def", "asc");
		asc = true;
	}

	// try reset previous sort styles
	var header = document.querySelectorAll("th.asc, th.desc");
	for(var i =0; header && i < header.length; i++){
		header[i].className = header[i].className.replace("asc", "").replace("desc", "");
		header[i].className += " def";
	}

	tgt.className = cssClass;
	
	var prop = tgt.getAttribute("data-key");// the property we will need to sort
	switch(prop){
		case "salary":
			tableProp.data.sort(function(a, b){
				var res = Compare.salaryCompare(a.salary, b.salary);
				return asc ? res : -1 * res;
			});
			break;
		case "age":
			tableProp.data.sort(function(a, b){
				var res = Compare.numberCompare(a.age, b.age);
				return asc ? res : -1 * res;
			});
			break;
		case "startDate":
			tableProp.data.sort(function(a, b){
				var res = Compare.startDateCompare(a.startDate, b.startDate);
				return asc ? res : -1 * res;
			});
			break;
		default:// all the rest
			tableProp.data.sort(function(a, b){
				var res = Compare.basicCompare(a[prop], b[prop]);
				return asc ? res : -1 * res;
			});
			break;
	}
		
	buildTableData();

	// apply column style
	cells = document.querySelectorAll("td."+prop);
	for(var i = 0; i < cells.length; i++){
		cells[i].className += " hi";
	}
}

// handle hide column
function handleHide(evt){
	var tgt = evt.srcElement;
	var hide = tgt.checked;

	var prop = tgt.value;
	console.log("hide toggle clicked:"+prop);
	var cells = document.querySelectorAll("."+prop);

	
	for(var i = 0; i < cells.length; i++){
		if(hide) cells[i].className += " hide";
	 	else cells[i].className = cells[i].className.replace("hide", "");
	}
}

// handle change page size
function handlePageSizeChange(evt){
	var tgt = evt.srcElement;
	var val = tgt.value;
	tableProp.size = val;

	if(val == 100 && tableProp.data.length < 100){
		loadData().then(function(data){
			tableProp.data = tableProp.data.concat(JSON.parse(data));
			buildTableData();
			buildPagination(document.getElementById("datatable"));
		});
	}else{
		buildTableData();
		buildPagination(document.getElementById("datatable"));
	}
	
}

// handle user search
function handleSearch(evt){
	var tgt = evt.srcElement;
	var val = tgt.value.trim();
	var res = null;
	if(val != ""){
		res = [];
		for(var i = 0; i < tableProp.data.length; i++){
			// flatten the obj structure,so we can do simple serch, instead of loop through properties
			var str = JSON.stringify(tableProp.data[i]);
			if(str.search(new RegExp(val, "i")) >= 0){// search ignore case
				res.push(tableProp.data[i]);
			}
		}
	}

	// build table use search result	
	buildTableData(res);
}

// handle pagination 
function handlePaging(evt){
	var tgt = evt.srcElement;
	var page = tgt.getAttribute("data-page");// the page tp show
	var cur = tableProp.page ? tableProp.page : 0;
	if(page == "prev"){
		page = cur -1;
		if(page < 0) {//already at first page
			return;
		}
	}else if (page == "next"){
		page = cur +1;
		var max = Math.ceil(tableProp.data.length/tableProp.size);
		if(page > max){//already at last page
			return;
		}
	}

	tableProp.page = page;// set the page numer, and let the build function take care of it
	buildTableData();
}


/**
 *
 *  rendering functions 
 *
 */
// build pag size control
function buildPageSize(container){
       	var p = document.createElement("div");
	p.id = "search";
	var text = document.createTextNode("Page size:");
	p.appendChild(text);

	var dropdown = document.createElement("select");
	dropdown.addEventListener("change", handlePageSizeChange, false);
	 var sizes = tableProp.sizes;
	 for(var i = 0; i < sizes.length; i++){
	 	var op = document.createElement("option");
	 	op.innerText = sizes[i];
	 	op.value = sizes[i];
		dropdown.appendChild(op);
	 }
	p.appendChild(dropdown);
	container.appendChild(p);
}

// build search input
function buildSearch(container){
	var search = document.createElement("div");
	search.id = "search";
	var text = document.createTextNode("Search:");
	 search.appendChild(text);
	var input = document.createElement("input");
       	input.type = "text";
	input.className = "search";
	input.addEventListener("keyup", handleSearch, false);
	search.appendChild(input);
	container.appendChild(search);
}

// build table header
function buildTableHeader(table){
	var thead = document.createElement("thead");

	var i = 0;
	for(var p in tableProp.headers){
		var th = document.createElement("th");
		th.className = p+" col" + i + " def";
		th.setAttribute("data-key", p);
		th.innerText = tableProp.headers[p];	
		// add table header click event listener
		th.addEventListener("click", handleSort, false);
		thead.appendChild(th);
	}
	table.appendChild(thead);
}

// build pagination buttons
function buildPagination(container){
	var paging = document.querySelector(".paging");
	if(!paging){
		paging = document.createElement("div");
		paging.className = "paging";
		container.appendChild(paging);
	}
	paging.innerHTML = "";

	//var text = document.createElement("span");
	//text.innerText = "show ? of ???(not implemented)";
	//paging.appendChild(text);

	var prev = document.createElement("span");
	prev.innerText = "Previous";
	prev.className = "page";
	prev.setAttribute("data-page", "prev");
	prev.addEventListener("click", handlePaging, false);
	paging.appendChild(prev);
	
	var len = tableProp.data.length;
	var size = tableProp.size;
	var count = Math.ceil(len/size);
	
	for(var i = 1; i <= count; i++){
		var e = document.createElement("span");
		e.innerText = i;
		e.className = "page";
		e.setAttribute("data-page", i-1);
		e.addEventListener("click", handlePaging, false);
		paging.appendChild(e);
	}
	var next = document.createElement("span");
	next.innerText = "Next";
	next.className = "page";
	next.setAttribute("data-page", "next");
	next.addEventListener("click", handlePaging, false);
	paging.appendChild(next);
	
}

// build table rows, either use data: from param (search result),
// or: use cached data
function buildTableData(data){
	var tbody = document.getElementsByTagName("tbody")[0];
	tbody.innerHTML = "";
	// check is data is passed in, if not use cached data
	var data = data ? data : tableProp.data;
	var start = tableProp.page ? tableProp.page * tableProp.size : 0;
	for(var i = start; i < start + tableProp.size; i++){
		var obj = data[i];
		if(!obj) break;
		var row = document.createElement("tr");
	 	var idx = 0;
		for(var p in obj){
			var td = document.createElement("td");
			td.innerText = obj[p];
			td.className = p+ " col"+idx;
	 		row.appendChild(td);
	 		idx++;
		}
		tbody.appendChild(row);
	}
}

// define data table properties
// uses to build table, page size, pagination sizes etc
// TODO: currently doesn't save sort info, so when search or page size change, sort info is not retained
var tableProp = {
	sizes: [10, 30, 50, 100],// number of rows per page
	size: 10,
	headers: {name: "Name" , position: "Position", office: "Office", age: "Age", startDate: "Start Date", salary: "Salary"},
	// some fake data to start with
	data: [{"name":"Airi Satou","position":"Accountant","office":"Tokyo","age":"33","startDate":"2008/11/28","salary":"$162,700"},{"name":"Angelica Ramos","position":"Chief Executive Officer (CEO)","office":"London","age":"47","startDate":"2009/10/09","salary":"$1,200,000"},{"name":"Ashton Cox","position":"Junior Technical Author","office":"San Francisco","age":"66","startDate":"2009/01/12","salary":"$86,000"},{"name":"Bradley Greer","position":"Software Engineer","office":"London","age":"41","startDate":"2012/10/13","salary":"$132,000"},{"name":"Brenden Wagner","position":"Software Engineer","office":"San Francisco","age":"28","startDate":"2011/06/07","salary":"$206,850"},{"name":"Brielle Williamson","position":"Integration Specialist","office":"New York","age":"61","startDate":"2012/12/02","salary":"$372,000"},{"name":"Bruno Nash","position":"Software Engineer","office":"London","age":"38","startDate":"2011/05/03","salary":"$163,500"},{"name":"Caesar Vance","position":"Pre-Sales Support","office":"New York","age":"21","startDate":"2011/12/12","salary":"$106,450"},{"name":"Cara Stevens","position":"Sales Assistant","office":"New York","age":"46","startDate":"2011/12/06","salary":"$145,600"},{"name":"Cedric Kelly","position":"Senior Javascript Developer","office":"Edinburgh","age":"22","startDate":"2012/03/29","salary":"$433,060"},{"name":"Charde Marshall","position":"Regional Director","office":"San Francisco","age":"36","startDate":"2008/10/16","salary":"$470,600"},{"name":"Colleen Hurst","position":"Javascript Developer","office":"San Francisco","age":"39","startDate":"2009/09/15","salary":"$205,500"},{"name":"Dai Rios","position":"Personnel Lead","office":"Edinburgh","age":"35","startDate":"2012/09/26","salary":"$217,500"},{"name":"Donna Snider","position":"Customer Support","office":"New York","age":"27","startDate":"2011/01/25","salary":"$112,000"},{"name":"Doris Wilder","position":"Sales Assistant","office":"Sidney","age":"23","startDate":"2010/09/20","salary":"$85,600"},{"name":"Finn Camacho","position":"Support Engineer","office":"San Francisco","age":"47","startDate":"2009/07/07","salary":"$87,500"},{"name":"Fiona Green","position":"Chief Operating Officer (COO)","office":"San Francisco","age":"48","startDate":"2010/03/11","salary":"$850,000"},{"name":"Garrett Winters","position":"Accountant","office":"Tokyo","age":"63","startDate":"2011/07/25","salary":"$170,750"},{"name":"Gavin Cortez","position":"Team Leader","office":"San Francisco","age":"22","startDate":"2008/10/26","salary":"$235,500"},{"name":"Gavin Joyce","position":"Developer","office":"Edinburgh","age":"42","startDate":"2010/12/22","salary":"$92,575"},{"name":"Gloria Little","position":"Systems Administrator","office":"New York","age":"59","startDate":"2009/04/10","salary":"$237,500"},{"name":"Haley Kennedy","position":"Senior Marketing Designer","office":"London","age":"43","startDate":"2012/12/18","salary":"$313,500"},{"name":"Hermione Butler","position":"Regional Director","office":"London","age":"47","startDate":"2011/03/21","salary":"$356,250"},{"name":"Herrod Chandler","position":"Sales Assistant","office":"San Francisco","age":"59","startDate":"2012/08/06","salary":"$137,500"},{"name":"Hope Fuentes","position":"Secretary","office":"San Francisco","age":"41","startDate":"2010/02/12","salary":"$109,850"},{"name":"Howard Hatfield","position":"Office Manager","office":"San Francisco","age":"51","startDate":"2008/12/16","salary":"$164,500"},{"name":"Jackson Bradshaw","position":"Director","office":"New York","age":"65","startDate":"2008/09/26","salary":"$645,750"},{"name":"Jena Gaines","position":"Office Manager","office":"London","age":"30","startDate":"2008/12/19","salary":"$90,560"},{"name":"Jenette Caldwell","position":"Development Lead","office":"New York","age":"30","startDate":"2011/09/03","salary":"$345,000"},{"name":"Jennifer Acosta","position":"Junior Javascript Developer","office":"Edinburgh","age":"43","startDate":"2013/02/01","salary":"$75,650"},{"name":"Jennifer Chang","position":"Regional Director","office":"Singapore","age":"28","startDate":"2010/11/14","salary":"$357,650"},{"name":"Jonas Alexander","position":"Developer","office":"San Francisco","age":"30","startDate":"2010/07/14","salary":"$86,500"},{"name":"Lael Greer","position":"Systems Administrator","office":"London","age":"21","startDate":"2009/02/27","salary":"$103,500"},{"name":"Martena Mccray","position":"Post-Sales support","office":"Edinburgh","age":"46","startDate":"2011/03/09","salary":"$324,050"},{"name":"Michael Bruce","position":"Javascript Developer","office":"Singapore","age":"29","startDate":"2011/06/27","salary":"$183,000"},{"name":"Michael Silva","position":"Marketing Designer","office":"London","age":"66","startDate":"2012/11/27","salary":"$198,500"},{"name":"Michelle House","position":"Integration Specialist","office":"Sidney","age":"37","startDate":"2011/06/02","salary":"$95,400"},{"name":"Olivia Liang","position":"Support Engineer","office":"Singapore","age":"64","startDate":"2011/02/03","salary":"$234,500"},{"name":"Paul Byrd","position":"Chief Financial Officer (CFO)","office":"New York","age":"64","startDate":"2010/06/09","salary":"$725,000"},{"name":"Prescott Bartlett","position":"Technical Author","office":"London","age":"27","startDate":"2011/05/07","salary":"$145,000"},{"name":"Quinn Flynn","position":"Support Lead","office":"Edinburgh","age":"22","startDate":"2013/03/03","salary":"$342,000"},{"name":"Rhona Davidson","position":"Integration Specialist","office":"Tokyo","age":"55","startDate":"2010/10/14","salary":"$327,900"},{"name":"Sakura Yamamoto","position":"Support Engineer","office":"Tokyo","age":"37","startDate":"2009/08/19","salary":"$139,575"},{"name":"Serge Baldwin","position":"Data Coordinator","office":"Singapore","age":"64","startDate":"2012/04/09","salary":"$138,575"},{"name":"Shad Decker","position":"Regional Director","office":"Edinburgh","age":"51","startDate":"2008/11/13","salary":"$183,000"},{"name":"Shou Itou","position":"Regional Marketing","office":"Tokyo","age":"20","startDate":"2011/08/14","salary":"$163,000"},{"name":"Sonya Frost","position":"Software Engineer","office":"Edinburgh","age":"23","startDate":"2008/12/13","salary":"$103,600"},{"name":"Suki Burks","position":"Developer","office":"London","age":"53","startDate":"2009/10/22","salary":"$114,500"},{"name":"Tatyana Fitzpatrick","position":"Regional Director","office":"London","age":"19","startDate":"2010/03/17","salary":"$385,750"},{"name":"Thor Walton","position":"Developer","office":"New York","age":"61","startDate":"2013/08/11","salary":"$98,540"},{"name":"Tiger Nixon","position":"System Architect","office":"Edinburgh","age":"61","startDate":"2011/04/25","salary":"$320,800"},{"name":"Timothy Mooney","position":"Office Manager","office":"London","age":"37","startDate":"2008/12/11","salary":"$136,200"},{"name":"Unity Butler","position":"Marketing Designer","office":"San Francisco","age":"47","startDate":"2009/12/09","salary":"$85,675"},{"name":"Vivian Harrell","position":"Financial Controller","office":"San Francisco","age":"62","startDate":"2009/02/14","salary":"$452,500"},{"name":"Yuri Berry","position":"Chief Marketing Officer (CMO)","office":"New York","age":"40","startDate":"2009/06/25","salary":"$675,000"},{"name":"Zenaida Frank","position":"Software Engineer","office":"New York","age":"63","startDate":"2010/01/04","salary":"$125,250"},{"name":"Zorita Serrano","position":"Software Engineer","office":"San Francisco","age":"56","startDate":"2012/06/01","salary":"$115,000"}]

};

// build show/hide checkbox toggles
function buildHideToggle(container){
	var e = document.createElement("div");
	 e.id = "toggle";
	var text = document.createTextNode("Hide columns:");
	 e.appendChild(text);

	 for(var p in tableProp.headers){
		var span = document.createElement("span");
		var label = document.createTextNode(tableProp.headers[p]);
 		span.appendChild(label);	 
	 	
	 	var el = document.createElement("input");
	 	el.type = "checkbox";
	 	el.value = p;
		el.addEventListener("click", handleHide, false);
	 	
	 	span.appendChild(el);
	 	
	 	e.appendChild(span);
	 }
	 container.appendChild(e);
}



// Mr Data, set a course. Engage!
(
function initTable(){
	var table = document.createElement("table");
	
	var container = document.createElement("div");
	container.id = "datatable";

	buildPageSize(container);
	buildSearch(container);

	buildHideToggle(container);

	var tbody = document.createElement("tbody");
	table.appendChild(tbody);

	container.appendChild(table);
	document.body.appendChild(container);

	buildTableHeader(table);
	
	buildTableData();

	buildPagination(container);
})();


// load json data file from server
function loadData () {
  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "data.json");// get data file from current path
    xhr.onload = function () {
      if (this.status == 200) {
        resolve(xhr.response);
      } else {
        reject({
          status: this.status,
          statusText: xhr.statusText
        });
      }
    };
    xhr.onerror = function () {
    };
    xhr.send();
  });
}

