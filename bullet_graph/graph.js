import React from 'react/addons';

class BulletGraph extends React.Component {
  	/* calculates marker|scale size and labels
  		returns an array of marker labels */
	getMarkers(max){
		var marker = [];
		
		var unit = max / 1000000; //millions
		var prefix = "";
		var len = 0;
		if(unit >= 1){
			 unit = 1000000;
			 prefix = "M";
		} else if (max / 100000 >= 1) { //100k
			unit = 100;
			 prefix = "K";
			 len = max/100000;
		} else if (max/10000 >= 1){ // 10k
				unit = 10000;
			 prefix = "K";
			 len = max/10000;   
		}else if (max/1000 >= 1){ // 1k
			unit = 1000;
			prefix = "";
			len = max/1000;
		}else if (max/100 >= 1) {//100
			unit = 100;
			len = max/100;
		}else if (max/10 >= 1) { //10
			unit = 10;
			len = max/10;
		}else {
			unit = 1;	
			len = max;
		}

		for(var i =0; i < len; i++){
			var num = i*unit;
			if(num == 0) marker.push(0);	
			else {
				marker.push(i*unit+prefix);
			}	
		}
		return marker;
	}
	
	/* render function for all the markers */
	renderMarkers(arr){
		var tmp =[];
		for(var i =0; i < arr.length; i++){
			if(i == 0 ){
				tmp.push(<span className="number first">{arr[i]}</span>);	
			}else if (i == arr.length -1){
				tmp.push(<span className="number">{arr[i]}</span>);
			} else {
				tmp.push(<span className="mark"><span className="number">{arr[i]}</span></span>);
			}
		}
		return tmp;
	}
	
	/* render function for the component */
	render() {
		var ranges = this.props.data.ranges;
		var okWidth = Math.round(ranges.ok/ranges.good * 100) + "%";
		console.log("ok:"+ ranges.ok+"; good:"+ranges.good+"; %:"+okWidth);

		var badWidth = Math.round(ranges.bad/ranges.good * 100) + "%";
		console.log("bad:"+ ranges.bad+"; good:"+ranges.good+"; %:"+badWidth);
		
		var actualWidth = Math.round(this.props.data.actual/ranges.good * 100)+ "%";
		console.log("actual:"+ this.props.data.actual+"; good:"+ranges.good+"; %:"+actualWidth);

		var target = Math.round(this.props.data.target/ranges.good * 100) + "%"; 
		console.log("actual:"+ this.props.data.target+"; good:"+ranges.good+"; %:"+actualWidth);

		//console.log(this.props.data);
		var marks = this.getMarkers(ranges.good);
		console.log(marks);
		
		var marker = this.renderMarkers(marks);
		
    return (
<div className="bullet-graph">
	<div className="section-label">
		<div className="title">{this.props.data.title}</div>
		<div className="note">{this.props.data.notes}</div>
	</div>
	<div className="section-graph">
		<div className="ranges">
			<div className="ok" style={{width:okWidth}}></div>
			<div className="bad" style={{width:badWidth}}></div>
		</div>		
		<div className="actual" style={{width:actualWidth}}></div>
		<div className="target" style={{left:target}}></div>
		<div className="markers">
				{marker}
		</div>
	</div>
</div>
    );
  }
}